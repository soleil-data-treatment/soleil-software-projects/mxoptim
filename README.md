<!-- PROJECT LOGO -->
<br />
<div align="center">

<img src="images/logo.png" alt="Logo" width="80" height="80">

<p align="center">
    python script to optimize the arguments of <a href="http://www.mccode.org/">mcxtrace</a>
    <br />
    <!---
    <a href="https://github.com/othneildrew/Best-README-Template">View Demo</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues">Report Bug</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template"><strong>Explore the docs »</strong></a>
    -->
</p>
    
</div>



<!-- TABLE OF CONTENTS -->
<details>
<summary>

# Table of contents

</summary>

- [Table of contents](#table-of-contents)
- [About The Project](#about-the-project)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Add mxoptim from mcxtrace](#add-mxoptim-from-mcxtrace)
  - [Usage](#usage)
  - [Examples](#examples)
- [TODO logs (french)](#todo-logs-french)
- [Contact](#contact)

</details>



# About The Project

Mxoptim an independant script to optimize the arguments of [mcxtrace].

The script reads each parameters and optimize them with the [scipy] librairy by using the mcxtrace's outputs calls. From the output, the scipt extracts the detector line, reads the field value and use it for optimization. 

Example:
```json
Optimization terminated successfully.
         Current function value: -0.000000
         Iterations: 33
         Function evaluations: 78
execution time  116.58568668365479
args            result               uncertainty         
OMM             14.221155732870102   0.05604193048537368 
TTM             28.542428374290466   0.1006304758204136 
```

<div align="center">
<img align="center" src="images/plot.jpg" alt="Logo" width="500" height="350">
</div>

# Getting Started

## Prerequisites

- Install python3 (written in Python 3.9.7)

- Install mcxtrace

- (optional) [Install mcxtrace using docker]
    - Install docker (if needed)
    - Install the mcxtrace docker image
    ```sh
    docker run -p 8888:8888 docker.io/mccode/mcxtrace-1.7-3.0:1.0
    ```
    - Connect your browser to the URL communicated by the docker command

- Install the libraries needed by mxoptim
    ```sh
    pip install scipy
    ```

## Installation

- Copy/paste the content of `mxoptim.py`   

- Or clone the repo
    ```sh
    git clone https://gitlab.com/soleil-data-treatment/soleil-software-projects/mxoptim
    ```

## Add mxoptim from mcxtrace

- Using submodule as following
    ```sh
    git clone https://github.com/McStasMcXtrace/McCode.git
    cd McCode
    git checkout master
    git submodule init
    git submodule update
    ls tools/mxoptim
    ```

## Usage

```sh
usage: mxoptim.py [-h] [--maxiter maxiter] [--tol tol] [--method method] [--minimize] [--monitor monitor] [--no-output-files]
                  [--silent] [--verbose]
                  instr KEY=VALUE [KEY=VALUE ...]
```  
Mxoptim needs positional arguments which are:
```
  instr              instr file (required)
  KEY=VALUE          set a number of key-value pairs
```
* Where KEY=VALUE are your arguments you want to optimize. You can add optional arguments after the positional arguments.   
  
* Any argument not recognized by the scipt is used as mcxtrace arguments.
  
<details>

<summary>

* The full list of options can be obtained with `mxoptim -h` :

</summary>

```
usage: mxoptim.py [-h] [--maxiter maxiter] [--tol tol] [--method method]
                  [--minimize] [--monitor monitor] [--no-output-files]
                  [--silent] [--verbose] [--exec exec]
                  instr KEY=VALUE [KEY=VALUE ...]

Tool to optimise a beamline with McXtrace. Maximize the intensity.

positional arguments:
  instr              instr file (required)
  KEY=VALUE          set a number of key-value pairs

optional arguments:
  -h, --help         show this help message and exit
  --maxiter maxiter  max iter of optimization
  --tol tol          tolerance criteria to end the optimization
  --method method    Method to maximize the intensity in ['nelder-mead', 'powell', 'cg', 'bfgs', 'newton-cg', 'l-bfgs-b', 'tnc', 'cobyla', 'slsqp', 'trust-constr', 'dogleg', 'trust-ncg', 'trust-exact', 'trust-krylov']
                     (default: nelder-mead)
                     You can use your own method by entering something else, it will add it as a librairy. Please refer to scipy documentation for proper use of it:
                     https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html?highlight=minimize
  --minimize         choose to minimize the function if needed
  --monitor monitor  monitor name
  --no-output-files  no output files
  --silent           silent all print
  --verbose          print more information
  --exec exec        name of the executable, e.g. mcrun or mxrun

```

</details>

## Examples

###  One command example

<table>
<tr>
<td> input </td> <td> output </td>
</tr>
<tr>
<td> 

```sh
./mxoptim.py --no-output-files ./instr_test_files/Test_Mono.instr --monitor emon_zm OMM=14,14.5 TTM=28,29 lambda=1 -n 1e6
``` 

</td>
<td>

```json
Optimization terminated successfully.
         Current function value: -0.000000
         Iterations: 35
         Function evaluations: 83
execution time  159.4647409915924
Fit parameters of scipy optimize error: hess_inv
args            OMM                  TTM                 
result          14.22203739498218    28.544847759825643  
uncertainty     0.05419846168872965  0.09576776826421542
```

</td>
</tr>
</table>

- Here we want to optimize OMM and TTM from the `./instr_test_files/Test_Mono.instr`. We set boundaries for the OMM and TTM parameters, and a fixed parameter: lambda. 
- `--monitor` to only use the values of the `emon_zm` monitor from the output.
- We also use `-n 1e6 --mpi=4` as additional parameters for mcxtrace.

###  Other examples

You can find test examples in the `/instr_test_files/test.py` file.

<details>
<summary>

# TODO logs (french)

</summary>

- lun 7 Mars 2022: optimiser plusieurs fichiers avec un .instr et mcxtrace
- mar 8 Mars: suivre instructions mxoptim
- lun 14 Mars: suivre instructions mxoptim
- mer 16 Mars: faire la fonction optimiser
- lun 21 Mars: faire la fonction optimiser
- mar 22 Mars: traiter les x, séparer les x variables et fixes
- lun 28 Mars: traiter les x, séparer les x variables et fixes
- mer 30 Mars: faire fonctionner l'optimiseur, vérifier avec la documentation
- lun 4 Avril: demander comment boucler sur les x. est-ce qu'il faut optimiser chaque element séparement ?
- ven 29 Avril: 
    ``` 
    ['mxrun', '--no-output-files', '-n', '10000', 'SOLEIL_ROCK.instr', 'Etohit', '=', '15501.618033974844', ',', '16000.0'] 
        TypeError: unsupported operand type(s) for -: 'list' and 'list'
    ```
- lun 9 Mai: TODO fix: 
    ```
    INFO: No output directory specified (--dir)
    INFO: Using directory: "SOLEIL_ROCK_20220509_171638"
    INFO: Using existing c-file: ./SOLEIL_ROCK.c
    INFO: Using existing binary: ./SOLEIL_ROCK.out
    WARNING: Ignoring invalid parameter: "Etohit"
    WARNING: Ignoring invalid parameter: "15501.618033974844"
    ```
- mer 11 Mai: Optimiseur OK, boucle bien. Mise en place du moniteur ? A voir comment récupérer les résultats de l'optimiseur.
    Mise en place de limites (bounds).
- mar 14 Jun: 
    - garder les trajectoires et les mettre dans un fichier
    - faire une option no output files
    - faire une variable globales qui récupère le output des itérations
- ven 24 Jun:
  - enlever les crochets dans optimization.dat
  - vérifier si on spécifie 1 moniteur, qu'il ne lise que pour un moniteur
  - afficher une seule valeure pour le moniteur
  - avoir le delta sur les paramètres
  - afficher à la dernière ligne le resultat
  - estimate parameter uncertainty
  - https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html --> custom minimizer
  - vérifier si l'optimizeur entre en argument appartient à une librairie
  - https://stackoverflow.com/questions/43593592/errors-to-fit-parameters-of-scipy-optimize
  - calculer la tolérance avec scipy minimize
- lun 27 Jun:
  - avoir le delta sur les paramètres
  - estimate parameter uncertainty
  - https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html --> custom minimizer
  - vérifier si l'optimizeur entre en argument appartient à une librairie
  - https://stackoverflow.com/questions/43593592/errors-to-fit-parameters-of-scipy-optimize
  - calculer la tolérance avec scipy minimize
- lun 4 Jul:
  - avoir le delta sur les paramètres
  - estimate parameter uncertainty
  - https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html --> custom minimizer
  - vérifier si l'optimizeur entre en argument appartient à une librairie
  - https://stackoverflow.com/questions/43593592/errors-to-fit-parameters-of-scipy-optimize
  - calculer la tolérance avec scipy minimize
  - mettre sur mcxtrace
- mar 13 Sep:
  - 🔴 avoir le delta sur les paramètres - estimate parameter uncertainty
  - 🔴 https://stackoverflow.com/questions/43593592/errors-to-fit-parameters-of-scipy-optimize
  - 🔴 calculer la tolérance avec scipy minimize
  - 🔴 Déterminer les barres erreur
  - 🟢 signature correcte en texte | vérifier si l'optimizeur entre en argument appartient à une librairie
  - 🟠 à tester, No module named 'scipy.optimize.basinhopping' | https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html --> custom minimizer
  - 🟢 (commande dans la doc) | submodule init/submodule update sur mcxtrace
  - 🟢 Mettre en documentation, quand on travail dans mcxtrace: 
    ```
        git clone https://github.com/McStasMcXtrace/McCode.git
        cd McCode
        git checkout master
        git submodule init
        git submodule update
        ls tools/mxoptim
    ```
  - 🟢 Donner liste des .instr dispos
  - 🟢 Réparer mxoptim dans find detector 
- mar 27 sept:
  - optimizers: python3-genetic python3-swarms
  - voir: https://github.com/ljvmiranda921/pyswarms/
  - mxioptim.py: ligne ~190: tester avec --method "pyswarms.single.GlobalBestPSO(n_particles=10, dimensions=2, options={'c1': 0.5, 'c2': 0.3, 'w':0.9})"
  - 🟢 mxoptim.py: lign 233: test McXtrace_args is None -> ne pas ajouter 'args='
  - chercher des exemples d'utilisation de scipy.optimize.minimize custom.
- mar 18 oct:
  - 🟢 try except: si hess_inv exist, extraire ftol de minimizer
  - 2nd, 3rd collone de optimization.dat -> historique 
  - script matlab à réécrire en python en utilisation optimisation.dat:
    output.criteriaHistory = last column of optimisation.dat
    output.parsHistory = columns 2:n-1 de optimisation.dat
    ```matlab
    % estimate parameter uncertainty from the search trajectory ====================

    

    index      = find(output.criteriaHistory < min(output.criteriaHistory)*4);   % identify tolerance region around optimum 
    if length(index) < 3 % retain 1/4 lower criteria part
    delta_criteria = output.criteriaHistory - min(output.criteriaHistory);
    index      = find(abs(delta_criteria/min(output.criteriaHistory)) < 0.25);
    end
    if length(index) < 3
    index = 1:length(output.criteriaHistory);
    end
    try
    delta_pars = (output.parsHistory(index,:)-repmat(output.parsBest,[length(index) 1])); % get the corresponding parameter set
    weight_pars= exp(-((output.criteriaHistory(index)-min(output.criteriaHistory))).^2 / 8); % Gaussian weighting for the parameter set
    weight_pars= repmat(weight_pars,[1 length(output.parsBest)]);
    output.parsHistoryUncertainty = sqrt(sum(delta_pars.*delta_pars.*weight_pars)./sum(weight_pars));
    end
    ```
    http://ifit.mccode.org/Optimizers.html#mozTocId228403
  - custom minimizer: ghttps://people.duke.edu/~ccc14/sta-663/BlackBoxOptimization.html
- mer 30 nov:
  - 415: divide by zero encountered in true_divide
  - 419: ValueError: The truth value of an array with more than one element is ambiguous. Use a.any() or a.all()
- mer 4 jan:
  - pickle for McXtrace_EstimateUncertainty
  - ValueError: The truth value of an array with more than one element is ambiguous. Use a.any() or a.all()
</details>



# Contact

Aurélien Castel - Emmanuel Farhi

Soleil Software Projects: https://gitlab.com/soleil-data-treatment/soleil-software-projects

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- LINKS -->
[mcxtrace]: http://www.mccode.org/
[scipy]: https://docs.scipy.org/doc/scipy/index.html
[Install mcxtrace using docker]: https://www.mcxtrace.org/#New%20ready-to-run%20McXtrace%20box%20in%20a%20browser
