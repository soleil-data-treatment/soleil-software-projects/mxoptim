
import os, sys, subprocess, pickle
  
# setting path
sys.path.insert(0, '..')
import mxoptim

def test_substr_return1():
    result = subprocess.run(["ls", "-l"], stdout=subprocess.PIPE, text=True)
    print(result.stdout)
    value, line = mxoptim.find_dectector(result.stdout, "README.md")
    print(value, line)

def test_substr_return2():
    cmd = ['mxrun', '--no-output-files', '-n', '10000', 'SOLEIL_ROCK.instr', 'Etohit=15500']
    result = subprocess.run(cmd, stdout=subprocess.PIPE, text=True)
    print(result.stdout)
    value, line = mxoptim.find_dectector(result.stdout, "Detector:")
    print(value, line)

def test_McXtrace_Plot(dt_string):
    with open("../" + dt_string + "/" + "global_result.pkl", "rb") as f1, open("../" + dt_string + "/" + "resultx.pkl", "rb") as f2:
        p1 = pickle.load(f1)
        p2 = pickle.load(f2)
        mxoptim.McXtrace_Plot(p1, p2)

def test_McXtrace_EstimateUncertainty(dt_string):
    with open("../" + dt_string + "/" + "global_result.pkl", "rb") as f1, open("../" + dt_string + "/" + "resultx.pkl", "rb") as f2:
        p1 = pickle.load(f1)
        p2 = pickle.load(f2)
        mxoptim.McXtrace_EstimateUncertainty(p1, p2)

def test_optim():

    # ---------------------------------------------------------------- uncomment a line to test it using test.py
    # -------------------------------- regular uses
    os.system("../mxoptim.py ./instr_test_files/Test_Mono.instr --monitor emon_zm OMM=14,14.5 TTM=28,29 lambda=1 -n 1e6")
    # os.system("../mxoptim.py --no-output-files ./instr_test_files/Test_Mono.instr --monitor emon_zm OMM=14,14.5 TTM=28,29 lambda=1 -n 1e6")

    # SOLEIL_ROCK, SOLEIL_ROCK2 --> no output ?
    # os.system("../mxoptim.py --verbose -no-output-files ./instr_test_files/SOLEIL_ROCK.instr Etohit=15000,16000 sample_density=7,10") # -n 1e6 --mpi=4
    # os.system("../mxoptim.py --verbose --no-output-files ./instr_test_files/SOLEIL_ROCK2.instr Etohit=15000,16000 sample_density=7,10") # -n 1e6 --mpi=4
    # os.system("../mxoptim.py --verbose --no-output-files --method powell ./instr_test_files/SOLEIL_ROCK2.instr Etohit=15000,16000 sample_density=7,10")

    # -------------------------------- custom methods
    # os.system("../mxoptim.py --verbose --method scipy.optimize.basinhopping ./instr_test_files/Test_Mirror_toroid.instr gamma=0,10")
    # os.system('../mxoptim.py ./instr_test_files/Test_Mirror_toroid.instr gamma=0,10 --verbose --method pyswarms.single.GlobalBestPSO\(n_particles=10,dimensions=2,options=\{\\"c1\\":0.5,\\"c2\\":0.3,\\"w\\":0.9\}\)')
    
    # -------------------------------- bash example
    # bash: ../mxoptim.py ./instr_test_files/Test_Mirror_toroid.instr gamma=0,10 --verbose --method pyswarms.single.GlobalBestPSO\(n_particles=10,dimensions=2,options=\{\"c1\":0.5,\"c2\":0.3,\"w\":0.9\}\)

    # ---------------------------------------------------------------- function tests
    # test_substr_return1()
    # test_substr_return2()
    # test_McXtrace_Plot("04012023_163302")
    # test_McXtrace_EstimateUncertainty("04012023_163302")

    pass
    
from pathlib import Path

if __name__ == '__main__':

    test_optim()

    # delete files
    for f in Path("..").glob("*.*"):
        filename = f.name
        if filename.endswith(".c") or filename.endswith(".instr") or filename.endswith(".out"):
            f.unlink()
    
    """
    regarder les paramètres
    dteta=0, radius_paramater_m2b=5e3, radius_paramater_toroid_m1=0.0317, radius_o_paramater_toroid_m1=9.02e3, angle_m1=0.0045, Etohit=15918, angle_m2a_m2b=0.008, nbsample = 1, string sample_file="Cu.txt", sample_density=8.96, string sample_file_2="Mo.txt", sample_density_2=10.28, string reflec_material_M1="Ir.dat",string reflec_material_M2A="B4C.dat", string reflec_material_M2B="B4C.txt", cc=0
    - problème optimization qui revient sur les points de milieu initiaux
    - montrer l'intensité (à la ligne detector) pendant l'execution
    - trouver un moyen pour faire des tests facilement (passer les arguments) 
    """
