#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  usage: mxoptim.py [-h] [--maxiter maxiter] [--tol tol] [--method method]
#                    [--minimize] [--monitor monitor] [--no-output-files]
#                    [--silent] [--verbose] [--exec exec]
#                    instr KEY=VALUE [KEY=VALUE ...]

#  Tool to optimise a beamline with McXtrace. Maximize the intensity.

#  positional arguments:
#    instr              instr file (required)
#    KEY=VALUE          set a number of key-value pairs

#  optional arguments:
#    -h, --help         show this help message and exit
#    --maxiter maxiter  max iter of optimization
#    --tol tol          tolerance criteria to end the optimization
#    --method method    Method to maximize the intensity in ['nelder-mead', 'powell', 'cg', 'bfgs', 'newton-cg', 'l-bfgs-b', 'tnc', 'cobyla', 'slsqp', 'trust-constr', 'dogleg', 'trust-ncg', 'trust-exact', 'trust-krylov']
#                       (default: nelder-mead)
#                       You can use your own method by entering something else, it will add it as a librairy. Please refer to scipy documentation for proper use of it:
#                       https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html?highlight=minimize
#    --minimize         choose to minimize the function if needed
#    --monitor monitor  monitor name
#    --no-output-files  no output files
#    --silent           silent all print
#    --verbose          print more information
#    --exec exec        name of the executable, e.g. mcrun or mxrun

# ---------- librairies ----------------
import time, argparse, sys, io
from os.path import exists
from xml.sax.handler import DTDHandler
from scipy.optimize import minimize, leastsq
import numpy as np
from datetime import datetime
import importlib
import textwrap
# ---------- global variables ----------
global_result = []
global_result_str = ""
now = datetime.now()
dt_string = now.strftime("%d%m%Y_%H%M%S") # ddmmYY_HMS
header = ""
# ----------

# ../mxoptim.py SOLEIL_ROCK.instr Etohit=15000,16000 sample_density=7,10 -n 1e6 --mpi=4
# ../mxoptim.py Test_Mono.instr --monitor emon_zm OMM=14,14.5 TTM=28,29 lambda=1 -n 1e6
# ---------- main =====================================================================
def main():
    """_summary_
        # objectif: maximiser le critère intensité\n
        /!\ pour le debugger bien se mettre dans le dossier mxoptim\n
    """

    start = time.time()
   
    MINIMIZE_METHODS = ['nelder-mead', 'powell', 'cg', 'bfgs', 'newton-cg',
                    'l-bfgs-b', 'tnc', 'cobyla', 'slsqp', 'trust-constr',
                    'dogleg', 'trust-ncg', 'trust-exact', 'trust-krylov']

    try: 
        os.chdir(os.path.dirname(__file__))
        script_path = os.path.dirname(os.path.realpath(__file__))
        instr_files_path = script_path + '/instr_test_files'
        list_instr = []
        list_instr += [each for each in os.listdir(instr_files_path) if each.endswith('.instr')]
    except FileNotFoundError:
        pass

    # -------------------------------- args handler
    parser = argparse.ArgumentParser(
        description='Tool to optimise a beamline with McXtrace. Maximize the intensity.', 
        formatter_class=argparse.RawTextHelpFormatter,
        epilog=textwrap.dedent('''\
            instr files in /instr_test_files:
            ''' +
            str(list_instr)
            )
        )
    # ---------- required
    parser.add_argument(
        "instr",
        metavar="instr",
        type=argparse.FileType('r', encoding='UTF-8'), # https://stackoverflow.com/questions/15203829/python-argparse-file-extension-checking
        help="instr file (required)",
        nargs=1,
        action=CheckExt(['instr'])
    )
    parser.add_argument(
        "params",
        metavar="KEY=VALUE",
        nargs='+',
        help="set a number of key-value pairs",
        action=ParseDict,
    )
    # ---------- options
    parser.add_argument(
        "--maxiter",
        metavar="maxiter",
        type=int,
        help="max iter of optimization",
        nargs=1,
    )
    parser.add_argument(
        "--tol",
        metavar="tol",
        type=float,
        help="tolerance criteria to end the optimization",
        nargs=1,
    )
    parser.add_argument(
        "--method",
        metavar='method',
        type=str,
        help='Method to maximize the intensity in ' + str(MINIMIZE_METHODS) + '\n' +
        '(default: ' + MINIMIZE_METHODS[0] + ')' + '\n' +
        'You can use your own method by entering something else, it will add it as a librairy. Please refer to scipy documentation for proper use of it:' + '\n' +
        'https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html?highlight=minimize',
        nargs=1,
        # action=CheckInList(MINIMIZE_METHODS),
        default=[MINIMIZE_METHODS[0]],
    )
    parser.add_argument(
        "--minimize",
        action='store_true',
        help='choose to minimize the function if needed',
    )
    parser.add_argument(
        "--monitor",
        metavar="monitor",
        type=str,
        help="monitor name",
        nargs=1,
        default="",
    )   
    parser.add_argument(
        "--no-output-files",
        action='store_true',
        help='no output files',
    )
    parser.add_argument(
        "--silent",
        action='store_true',
        help='silent all print',
    )
    parser.add_argument(
        "--verbose",
        action='store_true',
        help='print more information',
    )
    parser.add_argument(
        "--exec",
        metavar="exec",
        type=str,
        help="name of the executable, e.g. mcrun or mxrun",
        nargs=1,
        default="auto",
    )

    # --- parser args handling
    args, unknown_args = parser.parse_known_args()
    McXtrace_args = McXtrace_Init(args, unknown_args)

    # import method 
    if args.method[0] not in MINIMIZE_METHODS:
        try:            
            import importlib
            p, m = args.method[0].split('.', 1) # split text with each "."
            exec('import ' +  p)
            args.method = eval(args.method[0])
        except Exception as e:
            print("Import custom module error:", e)
            exit(-1)
    else:
        args.method = args.method[0]

    # show options optimizer, 
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.show_options.html#scipy.optimize.show_options
    # savoir les options qu'il y a besoins
    # show_options(solver="minimize", method="Nelder-Mead", disp=False)

    # print behaviors
    if args.silent:
        sys.stdout = io.StringIO()

    if args.verbose:
        print('{:<15} {:^0}'.format("args", str(args)))
        print('{:<15} {:^0}'.format("McXtrace_args", str(McXtrace_args)))

    bounds = [] # https://stackoverflow.com/questions/43702352/maximize-optimization-using-scipy
    for i in McXtrace_args['params']:
        bounds.append((McXtrace_args['params'][i]))

    print("\n# Criteria parameters\n")
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html
    
    # handle options
    options={'disp':True}
    if args.maxiter:
        options["maxiter"] = args.maxiter[0]
    if args.tol:
        options["tol"] = args.tol[0]
    # ---

    global header, global_result_str
    header += "# Instrument-source: '" + str(args.instr.name) + "'" + "\n"
    header += "# Date: " + now.strftime("%A %B %d %m %Y %H:%M:%S") + "\n"
    header += "# Param: " + ", ".join(f"{k}:{v}" for k, v in args.params.items()) + "\n"
    header += "# title: Optimization of " + ", ".join(list(args.params.keys())) + "\n"
    header += "# xlabel: 'iteration'" + "\n"
    header += "# ylabel: 'Intensity'" + "\n"
    header += "# xvars: " + ", ".join(list(args.params.keys())) + "\n"
    header += "# yvars: " + ", ".join(list(args.monitor)) + "\n"
    header += "# filename: mccode.dat" + "\n"
    header += "# variables: iter " + ", ".join(list(args.params.keys())) + " " + ", ".join(list(args.monitor)) + "\n"
        
    print('{:<3} {:<3} {:^0}'.format("", "[arguments getting optimized]", "[values used for optimization, found in the output at the 'Dectector:' line]"))

    # === minimizing
    # case for handling custom optmizers
    if callable(args.method):
      print("Optimizer method is custom: "+str(args.method))
    else:
      print("Optimizer method is "+str(args.method))
    
    # call to minimize must forward additional arguments, such as in:
    # minimize(fun, start, method=method, **options)
    # where **options is 1st extracted between ( and ) from the command line, 
    # then evaluated as a dist, and expanded as argument.
    # one solution is to eval("minimize(....' + options_dict + ')'")
    result = minimize(
        McXtrace_Optimizer, McXtrace_args['start'],
        args=McXtrace_args,
        method=args.method, bounds=bounds,
        options=options)

    if args.verbose:
        print('{:<15} {:^0}'.format("result", str(result)))
    
    if args.silent:
        sys.stdout = sys.__stdout__    

    resultx_str = format(" ".join(map(str,result.x)))
    execution_time_str = str(time.time()-start)

    print('{:<15} {:^0}'.format("execution time", execution_time_str))

    McXtrace_FitErrors(result)

    import pickle 
    if not args.no_output_files:
        with open("./" + dt_string + "/" + "optimization.dat", "w") as f1, open("./" + dt_string + "/" + "global_result.pkl", "wb") as f2, open("./" + dt_string + "/" + "resultx.pkl", "wb") as f3:
            f1.write(header)
            f1.write(global_result_str)
            f1.write('{:<3} {:^0}'.format("res", resultx_str))

            # Serialize the object and write it to the file
            pickle.dump(global_result, f2) 
            pickle.dump(result.x, f3)

    # === plot
    if not args.no_output_files:
        McXtrace_Plot(global_result, result.x, list(args.params.keys()) + ["intensity"], dt_string)

    # === uncertainties
    uncertainties = McXtrace_EstimateUncertainty(global_result, result.x)
    # Combine the lists into a single list of tuples
    # Print the header
    # Print the combined list
    combined_list = list(zip(list(args.params.keys()), result.x, uncertainties["parsHistoryUncertainty"]))
    print('{:<10} {:<20} {:<20}'.format("args", "result", "uncertainty"))
    for x in combined_list:
        print('{:<10} {:<20} {:<20}'.format(*x))


# ---------------------------------------------------------------- optimizer
import subprocess
Nfeval = 1

def McXtrace_Optimizer(x, args):
    global Nfeval, global_result, global_result_str, header

    cmd = args['cmd'].copy()
    if args['no-output-files']:
        cmd.append("--no-output-files")
    else:
        try:
            os.mkdir("./" + dt_string)
        except OSError:
            pass
        cmd.append("--dir " + "./" + dt_string + "/" + str(Nfeval))
      
    if args['verbose']:
        print('{:<15} {:^0}'.format("cmd", str(cmd)))
        print('{:<15} {:^0}'.format("args", str(args)))
        print('{:<15} {:^0}'.format("x", str([float(item) for item in x])))

    for index, param in enumerate(args['params']):
        if args['verbose']:
            print('{:<15} {:^0}'.format("index", str(index)))
            print('{:<15} {:^0}'.format("param", str(param)))
        cmd.append(param + "=" + str(x[index]))

    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, text=True)
    process.wait()
    result = process.stdout.read()
    cmd.clear() # clean cmd because it's used in multi-threaded mode ?
    if args['verbose']:
        print('{:<15} {:^0}'.format("result", str(result)))

    values = find_dectector(result, args['monitor']) # value = grep(stdout, 'Detector:'+args.monitor)
    values = [float(ele) for ele in values] # convert list of str to list of float

    result_str = '{:<3} {:<3} {:^0}'.format(Nfeval, " ".join(map(str,x)), " ".join(map(str,values)))
    print(result_str)
    global_result_str += result_str + '\n'
    global_result.append(x.tolist() + values)

    if not args['no-output-files']:
        if not exists("./" + dt_string + "/" + str(Nfeval)):
            os.mkdir("./" + dt_string + "/" + str(Nfeval))
        with open("./" + dt_string + "/" + str(Nfeval) + "/" + "optimization.dat", "w") as f:
            f.write(header)
            f.write(result_str)
    # add line in optimization.dat when not --no-output-files
    
    Nfeval += 1    
    if args['minimize']:
        return sum(values)
    else:
        return -sum(values) # maximize

# ---------------------------------------------------------------- optimizer initer
def McXtrace_Init(args, unknown_args):
    # init de la liste pour la cmd

    cmd = []
    exe = args.exec
    if exe == "auto":
      if 'MCSTAS' in os.environ:
        exe = "mcrun"
      elif 'MCXTRACE' in os.environ:
        exe = "mxrun"
      else:
        exe = "mxrun"
        
    cmd.append(exe) # e.g. mxrun or mxrun
    
    
    # TODO:optional: should be given as unsupported args
    for i in unknown_args:
        cmd.append(i)

    cmd.append(str(args.instr.name))

    start = []
    min_values = []
    max_values = []
    names = [] # list of tuples (key, value)

    for key, value in args.params.items():

        if isinstance(value, list): # variable: find start, min, max and name of variables to optimize
            if len(value) == 3: # min, start, max
                start.append(value[1])
                min_values.append(value[0])
                max_values.append(value[2])
                names.append((key, [value[0], value[1], value[2]]))
            if len(value) == 2: # min, max
                start.append((value[0] + value[1])/2)
                min_values.append(value[0])
                max_values.append(value[1])
                names.append((key, [value[0], value[1]]))

        else: # fixed
            names.append((key, value))
            cmd.append(str(key) + "=" + str(value)) # fixed parameter, or option for the instrument (--blah)str(key) + "=" + str(value) x
    
    McXtrace_initer = {} # everything needed to loop and make the command
    McXtrace_initer["params"] = args.params
    McXtrace_initer["monitor"] = args.monitor
    McXtrace_initer["start"] = start
    McXtrace_initer["min_values"] = min_values
    McXtrace_initer["max_values"] = max_values
    McXtrace_initer["names"] = names
    McXtrace_initer["minimize"] = args.minimize
    McXtrace_initer["cmd"] = cmd
    McXtrace_initer["verbose"] = args.verbose
    McXtrace_initer["no-output-files"] = args.no_output_files
    McXtrace_initer["unknown_args"] = unknown_args
    return McXtrace_initer # return (cmd, start -> x0, min, max, names)

# ---------------------------------------------------------------- after optimization processing 
def McXtrace_FitErrors(res):
    # errors to fit parameters of scipy optimize
    # https://stackoverflow.com/questions/43593592/errors-to-fit-parameters-of-scipy-optimize
    # np.diag(res.hess_inv.todense())
    try: 
        ftol = 2.220446049250313e-09
        tmp_i = np.zeros(len(res.x))
        for i in range(len(res.x)):
            tmp_i[i] = 1.0
            hess_inv_i = res.hess_inv(tmp_i)[i]
            uncertainty_i = np.sqrt(max(1, abs(res.fun)) * ftol * hess_inv_i)
            tmp_i[i] = 0.0
            print('x^{0} = {1:12.4e} ± {2:.1e}'.format(i, res.x[i], uncertainty_i))
    except Exception as e:
        print("Fit parameters of scipy optimize error:", e)

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
def McXtrace_Plot(res, result, ylabels, dt_string):

    data = np.array(res)

    fig, ax = plt.subplots()
    x = [i for i in range(1, len(data)+1)]

    colors = list(mcolors.TABLEAU_COLORS)

    arr = np.split(data, [1, 2], axis=1)
    for i, item in enumerate(arr):
        sub_ax = ax.twinx()
        sub_ax.plot(x, item, color=colors[i], label=ylabels[i])
        sub_ax.tick_params(axis="y", colors=colors[i])
        # ax2.legend()

    handles = []
    for i, l in enumerate(ylabels):
        handles.append(matplotlib.patches.Patch(color=colors[i], label=l))
    plt.legend(handles,ylabels, bbox_to_anchor=(0.85,1.025), loc="upper right")

    plt.title("MxOptim plot of " + str(ylabels))

    # Your plot code here
    plt.savefig("./" + dt_string + "/" + "plot.jpg", format='jpg') 


import math
def McXtrace_EstimateUncertainty(res, result):
    # estimate parameter uncertainty from the search trajectory
    # Chercher doc matlab
    # output.criteriaHistory --> intensité

    res = np.array(res)
    output = {
        "parsBest": np.array(result),
        "parsHistory": np.array(res[:,:2]),
        "parsHistoryUncertainty": [],
        "criteriaHistory": np.array(res[:,-1]),
    }

    delta_pars = output["parsHistory"] - output["parsBest"] # get the corresponding parameter set
    delta_criteria = output["criteriaHistory"] - output["criteriaHistory"].min()
    vect_exp = np.vectorize(math.exp)
    vect_sqrt = np.vectorize(math.sqrt)
    weight_pars = vect_exp(-(delta_criteria)**2 / 8) # gaussian weighting for the parameter set
    weight_pars = weight_pars.reshape(-1, 1)
    output["parsHistoryUncertainty"] = vect_sqrt(sum(delta_pars * delta_pars * weight_pars)/sum(weight_pars))
    """
    index = find(output.criteriaHistory < min(output.criteriaHistory)*4) # identify tolerance region around optimum 
    if len(index) < 3 % retain 1/4 lower criteria part
        delta_criteria = output.criteriaHistory - min(output.criteriaHistory);
        index      = find(abs(delta_criteria/min(output.criteriaHistory)) < 0.25);
    if len(index) < 3
        index = 1:len(output.criteriaHistory);
    delta_pars = (output.parsHistory(index,:)-repmat(output.parsBest,[length(index) 1])); # get the corresponding parameter set
    weight_pars= exp(-((output.criteriaHistory(index)-min(output.criteriaHistory))).^2 / 8); # gaussian weighting for the parameter set
    weight_pars= repmat(weight_pars,[1 length(output.parsBest)]);
    output.parsHistoryUncertainty = sqrt(sum(delta_pars.*delta_pars.*weight_pars)./sum(weight_pars));
    """
    # print('{:<15} {:^0}'.format("uncertainty", ' '.join(['{:<20}'.format(i) for i in output["parsHistoryUncertainty"]])))
    return output

# ================================================================= utilities
import re
def find_dectector(data: str, monitor = []) -> list:
    numbers = []
    DETECTOR_RE = r'Detector: ([^\s]+)_I=([^ ]+) \1_ERR=([^\s]+) \1_N=([^ ]+) "([^"]+)"'
    res = re.findall(DETECTOR_RE, data)

    # check if the monitor name exists
    for name, intensity, error, count, path in res:
        if name in monitor: # nom du moniteur
            numbers.append(intensity)

    # redo the loop (not so great)
    if len(numbers) == 0:
        for name, intensity, error, count, path in res:
            numbers.append(intensity)

    # if len(numbers) == 0:
    #     raise Exception("No monitor found from what you entered in parameters.")
    return numbers

class ParseDict(argparse.Action):
    """
    https://gist.github.com/fralau/061a4f6c13251367ef1d9a9a99fb3e8d

    Args:
        argparse (_type_): _description_
    """
    def __call__(self, parser, namespace, values, option_string=None):
        d = getattr(namespace, self.dest) or {}

        print(values)
        if values:
            for item in values:
                split_items = item.split("=", 1)
                key = split_items[0].strip()  # we remove blanks around keys, as is logical

                # convert values to either list of floats, list of ints or a string
                value = []
                try:
                    if ',' in split_items[1]:
                        value = [float(i) for i in split_items[1].split(',')]
                except ValueError:  # Not float
                    pass

                if not value:
                    try:
                        if ',' in split_items[1]:
                            value = [int(i) for i in split_items[1].split(',')]  
                    except ValueError: # Not int
                        pass

                if not value:
                    pass # must igore unsupported syntax
                else:
                    d[key] = value # store this variable to optimize
        setattr(namespace, self.dest, d)

import os
def CheckExt(choices):
    class Act(argparse.Action):
        def __call__(self, parser, namespace, userInput, option_string=None):
            elem = userInput[0]
            ext = os.path.splitext(elem.name)[1][1:]
            if ext not in choices:
                option_string = '({})'.format(option_string) if option_string else ''
                parser.error("doesn't correspond with one of {}{}".format(choices,option_string))
            else:
                setattr(namespace, self.dest, elem)
    return Act

def CheckInList(choices):
    class Act(argparse.Action):
        def __call__(self, parser, namespace, userInput, option_string=None):
            elem = userInput[0]
            if elem not in choices:
                option_string = '({})'.format(option_string) if option_string else ''
                parser.error("doesn't correspond with one of {}{}".format(choices,option_string))
            else:
                setattr(namespace, self.dest, elem)
    return Act

if __name__ == "__main__":
    main()
